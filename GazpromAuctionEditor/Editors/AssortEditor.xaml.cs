﻿using System;
using System.Windows.Controls;

namespace GazpromAuctionEditor.Editors
{
    /// <summary>
    /// Interaction logic for AssortEditor.xaml
    /// </summary>
    public partial class AssortEditor : UserControl
    {
        /// <summary>
        /// Ассортиментная позиция
        /// </summary>
        private Models.MasterCatalogChildren.AssortPosition _assortPosition;

        /// <summary>
        /// Доступ к элементу позиции
        /// </summary>
        public Models.MasterCatalogChildren.AssortPosition AssortPosition
        {
            get
            {
                return _assortPosition;
            }
            set
            {
                _assortPosition = value;
                UpdateControls();
            }
        }

        /// <summary>
        /// Публичный обработчик кнопки сохранения
        /// </summary>
        public EventHandler SaveClicked { get; set; }
        /// <summary>
        /// Публичный обработчик кнопки удаления
        /// </summary>
        public EventHandler DeleteClicked { get; set; }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public AssortEditor()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обновление контроллеров
        /// </summary>
        private void UpdateControls()
        {
            CodeTextBox.Text = _assortPosition.Code;
            NameTextBox.Text = _assortPosition.Name;
            DeliveryDaysLimitTextBox.Text = _assortPosition.DeliveryDaysLimit.ToString();
            PriceChangeDaysLimitTextBox.Text = _assortPosition.PriceChangeDaysLimit.ToString();
            ThresholdValueTextBox.Text = _assortPosition.ThresholdValue.ToString();
            ChangeIndicatorCurrencyTextBox.Text = _assortPosition.ChangeIndicatorCurrency;
            ChangeIndicatorCurrencyRatioTextBox.Text = _assortPosition.ChangeIndicatorCurrencyRatio.ToString();
        }

        /// <summary>
        /// Обработчик кнопки удаления
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DeleteClicked?.Invoke(this, e);
        }
        /// <summary>
        /// Обработчик кнопки сохранения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SaveClicked?.Invoke(this, e);
        }
    }
}
