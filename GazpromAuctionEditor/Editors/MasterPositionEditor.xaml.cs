﻿using System;
using System.Windows.Controls;

namespace GazpromAuctionEditor.Editors
{
    /// <summary>
    /// Interaction logic for MasterPositionEditor.xaml
    /// </summary>
    public partial class MasterPositionEditor : UserControl
    {
        /// <summary>
        /// Модель мастер позиции
        /// </summary>
        private Models.MasterCatalogChildren.MasterPosition _masterPosition;

        /// <summary>
        /// Редактируемая модель мастер позиции
        /// </summary>
        public Models.MasterCatalogChildren.MasterPosition MasterPosition
        {
            get
            {
                return _masterPosition;
            }
            set
            {
                _masterPosition = value;
                UpdateControls();
            }
        }

        /// <summary>
        /// Публичный обработчик кнопки сохранения
        /// </summary>
        public EventHandler SaveClicked { get; set; }
        /// <summary>
        /// Публичный обработчик кнопки удаления
        /// </summary>
        public EventHandler DeleteClicked { get; set; }

        /// <summary>
        /// Конструктор редактора
        /// </summary>
        public MasterPositionEditor()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Метод обновляющий данные в контроллере
        /// </summary>
        private void UpdateControls()
        {
            CodeTextBox.Text = _masterPosition.Code;
            NameTextBox.Text = _masterPosition.Name;
            DeliveryDaysLimitTextBox.Text = _masterPosition.DeliveryDaysLimit.ToString();
            ThresholdValueTextBox.Text = _masterPosition.ThresholdValue.ToString();
            ChangeIndicatorCurrencyTextBox.Text = _masterPosition.ChangeIndicatorCurrency;
            ChangeIndicatorCurrencyRatioTextBox.Text = _masterPosition.ChangeIndicatorCurrencyRatio.ToString();
            OKPD2TextBox.Text = _masterPosition.OKPD2;
            GostTextBox.Text = _masterPosition.Gost;
            UnitTextBox.Text = _masterPosition.Unit.ToString();
            PriceNoVatLimitTextBox.Text = _masterPosition.PriceNoVatLimit.ToString();
            PriceNoVatWithDeliveryLimitTextBox.Text = _masterPosition.PriceNoVatWithDeliveryLimit.ToString();
            PriceChangeDaysLimitTextBox.Text = _masterPosition.PriceChangeDaysLimit.ToString();
        }

        /// <summary>
        /// Обработчик кнопки удаления
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DeleteClicked?.Invoke(this, e);
        }
        /// <summary>
        /// Обработчик кнопки сохранения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SaveClicked?.Invoke(this, e);
        }
    }
}
