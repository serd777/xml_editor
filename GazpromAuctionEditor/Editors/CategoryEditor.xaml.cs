﻿using System;
using System.Windows.Controls;

namespace GazpromAuctionEditor.Editors
{
    /// <summary>
    /// Interaction logic for CategoryEditor.xaml
    /// </summary>
    public partial class CategoryEditor : UserControl
    {
        /// <summary>
        /// Выбранная категория
        /// </summary>
        private Models.MasterCatalogChildren.Category _category;

        /// <summary>
        /// Метод доступа к категории
        /// </summary>
        public Models.MasterCatalogChildren.Category Category
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
                UpdateControls();
            }
        }

        /// <summary>
        /// Публичный обработчик кнопки сохранения
        /// </summary>
        public EventHandler SaveClicked { get; set; }
        /// <summary>
        /// Публичный обработчик кнопки удаления
        /// </summary>
        public EventHandler DeleteClicked { get; set; }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public CategoryEditor()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Метод, обновляющий данные в контроллах
        /// </summary>
        private void UpdateControls()
        {
            NameMTRTextBox.Text = _category.NameMTR;
            CodeMTRTextBox.Text = _category.CodeMTR.ToString();
        }

        /// <summary>
        /// Обработчик кнопки удаления
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DeleteClicked?.Invoke(this, e);
        }
        /// <summary>
        /// Обработчик кнопки сохранения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SaveClicked?.Invoke(this, e);
        }
    }
}
