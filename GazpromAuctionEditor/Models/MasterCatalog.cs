﻿using System.Collections.Generic;

namespace GazpromAuctionEditor.Models
{
    public class MasterCatalog
    {
        public string MasterCatalogID { get; set; }
        public MasterCatalogChildren.Company Company { get; set; }
        public List<MasterCatalogChildren.Category> Categories { get; set; }
    }
}
