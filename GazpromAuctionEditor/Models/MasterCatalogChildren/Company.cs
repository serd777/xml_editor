﻿namespace GazpromAuctionEditor.Models.MasterCatalogChildren
{
    public class Company
    {
        public string Name  { get; set; }
        public string City  { get; set; }
        public string INN   { get; set; }
        public string KPP   { get; set; }
        public string Email { get; set; }
    }
}
