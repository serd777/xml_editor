﻿using System.Collections.Generic;

namespace GazpromAuctionEditor.Models.MasterCatalogChildren
{
    public class Category
    {
        public string NameMTR { get; set; }
        public double CodeMTR { get; set; }
        public List<MasterPosition> MasterPositions { get; set; }
    }
}
