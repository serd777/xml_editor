﻿using System.Collections.Generic;

namespace GazpromAuctionEditor.Models.MasterCatalogChildren
{
    public class AssortPosition
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int DeliveryDaysLimit { get; set; }
        public int PriceChangeDaysLimit { get; set; }
        public double ThresholdValue { get; set; }
        public string ChangeIndicatorCurrency { get; set; }
        public double ChangeIndicatorCurrencyRatio { get; set; }
        public List<Product> Products { get; set; }
        public MasterPosition Parent { get; set; }
    }
}
