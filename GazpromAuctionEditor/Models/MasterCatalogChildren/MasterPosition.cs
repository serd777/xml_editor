﻿using System.Collections.Generic;

namespace GazpromAuctionEditor.Models.MasterCatalogChildren
{
    public class MasterPosition
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int DeliveryDaysLimit { get; set; }
        public int PriceChangeDaysLimit { get; set; }
        public double ThresholdValue { get; set; }
        public string ChangeIndicatorCurrency { get; set; }
        public double ChangeIndicatorCurrencyRatio { get; set; }
        public string OKPD2 { get; set; }
        public string Gost { get; set; }
        public int Unit { get; set; }
        public double PriceNoVatLimit { get; set; }
        public double PriceNoVatWithDeliveryLimit { get; set; }
        public List<AssortPosition> AssortPositions { get; set; }
        public Category Parent { get; set; }
    }
}
