﻿using System.Xml;
using System.Collections.Generic;

namespace GazpromAuctionEditor.XML
{
    /// <summary>
    /// Класс формирования XML файла
    /// </summary>
    class Saver
    {
        /// <summary>
        /// Функция сохранения файла
        /// </summary>
        /// <param name="filePath">Путь до файла</param>
        /// <param name="masterCatalog">Данные для сохранения</param>
        public void SaveXML(string filePath, Models.MasterCatalog masterCatalog)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            settings.Indent = true;
            settings.IndentChars = "  ";

            var writer = XmlWriter.Create(filePath, settings);

            writer.WriteStartElement("MasterCatalog");

                //  Запишем ID каталога
                WriteMasterCatalogID(writer, masterCatalog);
                //  Запишем данные о компании
                WriteCompany(writer, masterCatalog);
                //  Запишем категории
                WriteCategories(writer, masterCatalog.Categories);

            writer.WriteEndElement();
            writer.Close();
        }

        /// <summary>
        /// Запись Master Catalog ID
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="masterCatalog"></param>
        private void WriteMasterCatalogID(XmlWriter writer, Models.MasterCatalog masterCatalog)
        {
            WriteXmlElement(writer, "MasterCatalogID", masterCatalog.MasterCatalogID);
        }
        /// <summary>
        /// Запись информации о компании
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="masterCatalog"></param>
        private void WriteCompany(XmlWriter writer, Models.MasterCatalog masterCatalog)
        {
            writer.WriteStartElement("Company");

                WriteXmlElement(writer, "Name",  masterCatalog.Company.Name);
                WriteXmlElement(writer, "City",  masterCatalog.Company.City);
                WriteXmlElement(writer, "INN",   masterCatalog.Company.INN);
                WriteXmlElement(writer, "KPP",   masterCatalog.Company.KPP);
                WriteXmlElement(writer, "Email", masterCatalog.Company.Email);

            writer.WriteEndElement();
        }
        /// <summary>
        /// Запись категорий
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="categories"></param>
        private void WriteCategories(XmlWriter writer, List<Models.MasterCatalogChildren.Category> categories)
        {
            writer.WriteStartElement("Categories");

            foreach (var category in categories)
                WriteCategory(writer, category);

            writer.WriteEndElement();
        }
        /// <summary>
        /// Запись категории
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="category"></param>
        private void WriteCategory(XmlWriter writer, Models.MasterCatalogChildren.Category category)
        {
            writer.WriteStartElement("Category");

                WriteXmlElement(writer, "NameMTR", category.NameMTR);
                WriteXmlElement(writer, "CodeMTR", category.CodeMTR.ToString().Replace(',', '.'));
                WriteMasterPositions(writer, category.MasterPositions);

            writer.WriteEndElement();
        }
        /// <summary>
        /// Запись списка MasterPositions
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="positions"></param>
        private void WriteMasterPositions(XmlWriter writer, List<Models.MasterCatalogChildren.MasterPosition> positions)
        {
            writer.WriteStartElement("MasterPositions");

            foreach (var position in positions)
                WriteMasterPosition(writer, position);

            writer.WriteEndElement();
        }
        /// <summary>
        /// Запись MasterPositions
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="position"></param>
        private void WriteMasterPosition(XmlWriter writer, Models.MasterCatalogChildren.MasterPosition position)
        {
            writer.WriteStartElement("MasterPosition");

                WriteXmlElement(writer, "Code", position.Code);
                WriteXmlElement(writer, "Name", position.Name);
                WriteXmlElement(writer, "DeliveryDaysLimit", position.DeliveryDaysLimit.ToString());
                WriteXmlElement(writer, "PriceChangeDaysLimit", position.PriceChangeDaysLimit.ToString());
                WriteXmlElement(writer, "ThresholdValue", position.ThresholdValue.ToString().Replace(',', '.'));
                WriteXmlElement(writer, "ChangeIndicatorCurrency", position.ChangeIndicatorCurrency);
                WriteXmlElement(writer, "ChangeIndicatorCurrencyRatio", position.ChangeIndicatorCurrencyRatio.ToString().Replace(',', '.'));
                WriteXmlElement(writer, "OKPD2", position.OKPD2);
                WriteXmlElement(writer, "Gost", position.Gost);
                WriteXmlElement(writer, "Unit", position.Unit.ToString());
                WriteXmlElement(writer, "PriceNoVatLimit", position.PriceNoVatLimit.ToString().Replace(',', '.'));
                WriteXmlElement(writer, "PriceNoVatWithDeliveryLimit", position.PriceNoVatWithDeliveryLimit.ToString().Replace(',', '.'));
                WriteAssortPositions(writer, position.AssortPositions);

            writer.WriteEndElement();
        }
        /// <summary>
        /// Запись списка AssortPositions
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="positions"></param>
        private void WriteAssortPositions(XmlWriter writer, List<Models.MasterCatalogChildren.AssortPosition> positions)
        {
            writer.WriteStartElement("AssortPositions");

            foreach (var position in positions)
                WriteAssortPosition(writer, position);

            writer.WriteEndElement();
        }
        /// <summary>
        /// Запись AssortPosition
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="position"></param>
        private void WriteAssortPosition(XmlWriter writer, Models.MasterCatalogChildren.AssortPosition position)
        {
            writer.WriteStartElement("AssortPosition");

                WriteXmlElement(writer, "Code", position.Code);
                WriteXmlElement(writer, "Name", position.Name);
                WriteXmlElement(writer, "DeliveryDaysLimit", position.DeliveryDaysLimit.ToString());
                WriteXmlElement(writer, "PriceChangeDaysLimit", position.PriceChangeDaysLimit.ToString());
                WriteXmlElement(writer, "ThresholdValue", position.ThresholdValue.ToString().Replace(',', '.'));
                WriteXmlElement(writer, "ChangeIndicatorCurrency", position.ChangeIndicatorCurrency);
                WriteXmlElement(writer, "ChangeIndicatorCurrencyRatio", position.ChangeIndicatorCurrencyRatio.ToString().Replace(',', '.'));

                //  ToDo: Add products saver if neccessary
                writer.WriteStartElement("Products");
                WriteXmlElement(writer, "Product");
                writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// Запись элемента XML
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="name"></param>
        /// <param name="data"></param>
        private void WriteXmlElement(XmlWriter writer, string name, string data = "")
        {
            writer.WriteStartElement(name);

            if(data != "")
                writer.WriteString(data);

            writer.WriteEndElement();
        }
    }
}
