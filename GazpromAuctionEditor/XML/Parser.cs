﻿using System.Collections.Generic;
using System.Xml;

namespace GazpromAuctionEditor.XML
{
    public class Parser
    {
        public Models.MasterCatalog ParseFile(string filePath)
        {
            var result = new Models.MasterCatalog();

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(filePath);

            //  Получаем корневой узел
            XmlElement xRoot = xDoc.DocumentElement;
            //  Обходим три элемента корневого узла
            foreach(XmlNode node in xRoot)
            {
                if (node.Name == "MasterCatalogID")
                    result.MasterCatalogID = node.InnerText;
                if (node.Name == "Company")
                    result.Company = ParseCompany(node);
                if (node.Name == "Categories")
                    result.Categories = ParseCategories(node);
            }

            return result;
        }

        /// <summary>
        /// Преобразование Company в модель
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private Models.MasterCatalogChildren.Company ParseCompany(XmlNode root)
        {
            var result = new Models.MasterCatalogChildren.Company();

            foreach(XmlNode node in root)
            {
                switch(node.Name)
                {
                    case "Name":  result.Name  = node.InnerText; break;
                    case "City":  result.City  = node.InnerText; break;
                    case "INN":   result.INN   = node.InnerText; break;
                    case "KPP":   result.KPP   = node.InnerText; break;
                    case "Email": result.Email = node.InnerText; break;
                    default: break;
                }
            }

            return result;
        }

        /// <summary>
        /// Преобразование Category в модель
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private List<Models.MasterCatalogChildren.Category> ParseCategories(XmlNode root)
        {
            var result = new List<Models.MasterCatalogChildren.Category>();

            foreach(XmlNode node in root)
            {
                var category = new Models.MasterCatalogChildren.Category();

                foreach(XmlNode c_node in node)
                {
                    switch(c_node.Name)
                    {
                        case "NameMTR": category.NameMTR = c_node.InnerText; break;
                        case "CodeMTR": category.CodeMTR = double.Parse(c_node.InnerText.Replace('.', ',')); break;
                        case "MasterPositions": category.MasterPositions = ParseMasterPositions(c_node, ref category); break;
                        default: break;
                    }
                }

                result.Add(category);
            }

            return result;
        }

        /// <summary>
        /// Преобразование в List<MasterPosition> модель
        /// </summary>
        /// <param name="root"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private List<Models.MasterCatalogChildren.MasterPosition> ParseMasterPositions(XmlNode root, ref Models.MasterCatalogChildren.Category parent)
        {
            var result = new List<Models.MasterCatalogChildren.MasterPosition>();

            foreach (XmlNode node in root)
                result.Add(ParseMasterPosition(node, ref parent));

            return result;
        }

        /// <summary>
        /// Преобразование в MasterPosition
        /// </summary>
        /// <param name="root"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        private Models.MasterCatalogChildren.MasterPosition ParseMasterPosition(XmlNode root, ref Models.MasterCatalogChildren.Category category)
        {
            var result = new Models.MasterCatalogChildren.MasterPosition
            {
                Parent = category
            };

            foreach (XmlNode node in root)
            {
                switch(node.Name)
                {
                    case "Code": result.Code = node.InnerText; break;
                    case "Name": result.Name = node.InnerText; break;
                    case "DeliveryDaysLimit": result.DeliveryDaysLimit = int.Parse(node.InnerText); break;
                    case "PriceChangeDaysLimit": result.PriceChangeDaysLimit = int.Parse(node.InnerText); break;
                    case "ThresholdValue": result.ThresholdValue = double.Parse(node.InnerText.Replace('.', ',')); break;
                    case "ChangeIndicatorCurrency": result.ChangeIndicatorCurrency = node.InnerText; break;
                    case "ChangeIndicatorCurrencyRatio": result.ChangeIndicatorCurrencyRatio = double.Parse(node.InnerText.Replace('.', ',')); break;
                    case "OKPD2": result.OKPD2 = node.InnerText; break;
                    case "Gost": result.Gost = node.InnerText; break;
                    case "Unit": result.Unit = int.Parse(node.InnerText); break;
                    case "PriceNoVatLimit": result.PriceNoVatLimit = double.Parse(node.InnerText.Replace('.', ',')); break;
                    case "PriceNoVatWithDeliveryLimit": result.PriceNoVatWithDeliveryLimit = double.Parse(node.InnerText.Replace('.', ',')); break;
                    case "AssortPositions": result.AssortPositions = ParseAssortPositions(node, ref result); break;
                    default: break;
                }
            }

            return result;
        }

        /// <summary>
        /// Преобразование в List<AssortPosition>
        /// </summary>
        /// <param name="root"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private List<Models.MasterCatalogChildren.AssortPosition> ParseAssortPositions(XmlNode root, ref Models.MasterCatalogChildren.MasterPosition parent)
        {
            var result = new List<Models.MasterCatalogChildren.AssortPosition>();

            foreach (XmlNode node in root)
                result.Add(ParseAssortPosition(node, ref parent));

            return result;
        }

        /// <summary>
        /// Преобразование в AssortPosition
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private Models.MasterCatalogChildren.AssortPosition ParseAssortPosition(XmlNode root, ref Models.MasterCatalogChildren.MasterPosition parent)
        {
            var result = new Models.MasterCatalogChildren.AssortPosition
            {
                Parent = parent
            };

            foreach (XmlNode node in root)
            {
                if (node.InnerText == "нет")
                    continue;
                switch(node.Name)
                {
                    case "Code": result.Code = node.InnerText; break;
                    case "Name": result.Name = node.InnerText; break;
                    case "DeliveryDaysLimit": result.DeliveryDaysLimit = int.Parse(node.InnerText); break;
                    case "PriceChangeDaysLimit": result.PriceChangeDaysLimit = int.Parse(node.InnerText); break;
                    case "ThresholdValue": result.ThresholdValue = double.Parse(node.InnerText); break;
                    case "ChangeIndicatorCurrencyRatio": result.ChangeIndicatorCurrencyRatio = double.Parse(node.InnerText.Replace('.', ',')); break;
                    default: break;
                }
            }

            return result;
        }
    }
}
