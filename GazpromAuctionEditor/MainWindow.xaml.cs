﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;

namespace GazpromAuctionEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Парсер XML файла
        /// </summary>
        private XML.Parser _parser;
        /// <summary>
        /// Парсер сохранения XML файла
        /// </summary>
        private XML.Saver _saver;

        /// <summary>
        /// Каталог аукциона
        /// </summary>
        private Models.MasterCatalog _masterCatalog;
        /// <summary>
        /// Редактор категорий
        /// </summary>
        private Editors.CategoryEditor _categoryEditor;
        /// <summary>
        /// Редактор позиций мастера
        /// </summary>
        private Editors.MasterPositionEditor _masterPositionEditor;
        /// <summary>
        /// Редактор ассортимент позиции
        /// </summary>
        private Editors.AssortEditor _assortEditor;

        /// <summary>
        /// Путь до открытого файла
        /// </summary>
        private string _filePath;

        /// <summary>
        /// Зависимость категории
        /// </summary>
        private static readonly DependencyProperty CategoryDependencyProperty = DependencyProperty.Register(
            "CategoryDependency",
            typeof(object),
            typeof(Models.MasterCatalogChildren.Category)
        );
        /// <summary>
        /// Зависимость мастер позиции
        /// </summary>
        private static readonly DependencyProperty MasterPositionDependencyProperty = DependencyProperty.Register(
            "MasterPositionDependency",
            typeof(object),
            typeof(Models.MasterCatalogChildren.MasterPosition)
        );
        /// <summary>
        /// Зависимость ассортимент позиции
        /// </summary>
        private static readonly DependencyProperty AssortPositionDependencyProperty = DependencyProperty.Register(
            "AssortPositionDependency",
            typeof(object),
            typeof(Models.MasterCatalogChildren.AssortPosition)
        );

        /// <summary>
        /// Конструктор окна
        /// </summary>
        public MainWindow()
        {
            _categoryEditor = new Editors.CategoryEditor();
            _masterPositionEditor = new Editors.MasterPositionEditor();
            _assortEditor = new Editors.AssortEditor();

            _parser = new XML.Parser();
            _saver = new XML.Saver();
            _masterCatalog = new Models.MasterCatalog();

            _categoryEditor.SaveClicked += SaveCategoryHandler;
            _masterPositionEditor.SaveClicked += SaveMasterHandler;
            _assortEditor.SaveClicked += SaveAssortHandler;

            _categoryEditor.DeleteClicked += DeleteCategoryHandler;
            _masterPositionEditor.DeleteClicked += DeleteMasterHandler;
            _assortEditor.DeleteClicked += DeleteAssortHandler;

            _filePath = "";

            InitializeComponent();
        }

        /// <summary>
        /// Инициализация контроллов
        /// </summary>
        private void InitControllers()
        {
            AuctionNameLabel.Content = _masterCatalog.MasterCatalogID + " - " + _masterCatalog.Company.Name;

            CategoriesTree.Items.Clear();
            //  Парсим категории в дерево
            foreach(Models.MasterCatalogChildren.Category category in _masterCatalog.Categories)
            {
                var categoryItem = new TreeViewItem();
                categoryItem.Header = category.CodeMTR + " - " + category.NameMTR;
                categoryItem.SetValue(CategoryDependencyProperty, category);

                //  Парсим Master позиции
                foreach(Models.MasterCatalogChildren.MasterPosition position in category.MasterPositions)
                {
                    var masterItem = new TreeViewItem();
                    masterItem.Header = position.Name;
                    masterItem.SetValue(MasterPositionDependencyProperty, position);

                    //  Парсим Assort позиции
                    foreach(Models.MasterCatalogChildren.AssortPosition assort in position.AssortPositions)
                    {
                        var assortItem = new TreeViewItem();
                        assortItem.Header = assort.Name;
                        assortItem.SetValue(AssortPositionDependencyProperty, assort);

                        masterItem.Items.Add(assortItem);
                    }

                    categoryItem.Items.Add(masterItem);
                }

                CategoriesTree.Items.Add(categoryItem);
            }
        }

        /// <summary>
        /// Обработчик клавиши загрузки файла
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                _filePath = openFileDialog.FileName;

                _masterCatalog = _parser.ParseFile(_filePath);
                InitControllers();
            }
        }

        /// <summary>
        /// Обработчик клавиши сохранения файла
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveFileButton_Click(object sender, RoutedEventArgs e)
        {
            _saver.SaveXML(_filePath, _masterCatalog);
            MessageBox.Show("Сохранено");
        }

        /// <summary>
        /// Обработчик клавиши "сохранить как" файла
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveAsFileButton_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Xml Files (*.xml)|*.xml";
            saveFileDialog.DefaultExt = "xml";
            saveFileDialog.AddExtension = true;

            if (saveFileDialog.ShowDialog() == true)
            {
                var fileName = saveFileDialog.FileName;
                _saver.SaveXML(fileName, _masterCatalog);
                MessageBox.Show("Сохранено");
            }
        }
        
        /// <summary>
        /// Обработчик смены выбранного элемента в дереве
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CategoriesTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            BodyGrid.Children.Clear();
            var item = (TreeViewItem) CategoriesTree.SelectedItem;

            if (item.GetValue(CategoryDependencyProperty) != null)
            {
                var category = (Models.MasterCatalogChildren.Category)item.GetValue(CategoryDependencyProperty);
                _categoryEditor.Category = category;

                BodyGrid.Children.Add(_categoryEditor);
            }
            else if(item.GetValue(MasterPositionDependencyProperty) != null)
            {
                var masterPosition = (Models.MasterCatalogChildren.MasterPosition)item.GetValue(MasterPositionDependencyProperty);
                _masterPositionEditor.MasterPosition = masterPosition;

                BodyGrid.Children.Add(_masterPositionEditor);
            }
            else if(item.GetValue(AssortPositionDependencyProperty) != null)
            {
                var assortPosition = (Models.MasterCatalogChildren.AssortPosition)item.GetValue(AssortPositionDependencyProperty);
                _assortEditor.AssortPosition = assortPosition;

                BodyGrid.Children.Add(_assortEditor);
            }
        }

        /// <summary>
        /// Обработчик сохранения MasterPosition
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SaveMasterHandler(object sender, EventArgs e)
        {
            var item = (TreeViewItem)CategoriesTree.SelectedItem;
            var masterPosition = (Models.MasterCatalogChildren.MasterPosition)item.GetValue(MasterPositionDependencyProperty);

            double.TryParse(_masterPositionEditor.ChangeIndicatorCurrencyRatioTextBox.Text, out double currencyIndicator);
            int.TryParse(_masterPositionEditor.DeliveryDaysLimitTextBox.Text, out int deliveryDays);
            int.TryParse(_masterPositionEditor.PriceChangeDaysLimitTextBox.Text, out int priceChangeDays);
            double.TryParse(_masterPositionEditor.ThresholdValueTextBox.Text, out double thresholdValue);
            int.TryParse(_masterPositionEditor.UnitTextBox.Text, out int unit);
            double.TryParse(_masterPositionEditor.PriceNoVatLimitTextBox.Text, out double priceNoVat);
            double.TryParse(_masterPositionEditor.PriceNoVatWithDeliveryLimitTextBox.Text, out double priceNoVatDelivery);

            masterPosition.Name = _masterPositionEditor.NameTextBox.Text;
            masterPosition.Code = _masterPositionEditor.CodeTextBox.Text;
            masterPosition.DeliveryDaysLimit = deliveryDays;
            masterPosition.PriceChangeDaysLimit = priceChangeDays;
            masterPosition.ThresholdValue = thresholdValue;
            masterPosition.ChangeIndicatorCurrency = _masterPositionEditor.ChangeIndicatorCurrencyTextBox.Text;
            masterPosition.ChangeIndicatorCurrencyRatio = currencyIndicator;
            masterPosition.OKPD2 = _masterPositionEditor.OKPD2TextBox.Text;
            masterPosition.Gost = _masterPositionEditor.GostTextBox.Text;
            masterPosition.Unit = unit;
            masterPosition.PriceNoVatLimit = priceNoVat;
            masterPosition.PriceNoVatWithDeliveryLimit = priceNoVatDelivery;

            item.Header = masterPosition.Name;
            MessageBox.Show("Сохранено");
        }
        /// <summary>
        /// Обработчик сохранения Category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SaveCategoryHandler(object sender, EventArgs e)
        {
            var item = (TreeViewItem)CategoriesTree.SelectedItem;
            var category = (Models.MasterCatalogChildren.Category)item.GetValue(CategoryDependencyProperty);

            category.NameMTR = _categoryEditor.CodeMTRTextBox.Text;
            category.NameMTR = _categoryEditor.NameMTRTextBox.Text;

            item.Header = category.CodeMTR + " - " + category.NameMTR;
            MessageBox.Show("Сохранено");
        }
        /// <summary>
        /// Обработчик сохранения AssortPosition
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SaveAssortHandler(object sender, EventArgs e)
        {
            var item = (TreeViewItem)CategoriesTree.SelectedItem;
            var assortPosition = (Models.MasterCatalogChildren.AssortPosition)item.GetValue(AssortPositionDependencyProperty);

            double.TryParse(_assortEditor.ChangeIndicatorCurrencyRatioTextBox.Text, out double currencyIndicator);
            int.TryParse(_assortEditor.DeliveryDaysLimitTextBox.Text, out int deliveryDays);
            int.TryParse(_assortEditor.PriceChangeDaysLimitTextBox.Text, out int priceChangeDays);
            double.TryParse(_assortEditor.ThresholdValueTextBox.Text, out double thresholdValue);

            assortPosition.Name = _assortEditor.NameTextBox.Text;
            assortPosition.Code = _assortEditor.CodeTextBox.Text;
            assortPosition.DeliveryDaysLimit = deliveryDays;
            assortPosition.PriceChangeDaysLimit = priceChangeDays;
            assortPosition.ThresholdValue = thresholdValue;
            assortPosition.ChangeIndicatorCurrency = _assortEditor.ChangeIndicatorCurrencyTextBox.Text;
            assortPosition.ChangeIndicatorCurrencyRatio = currencyIndicator;

            item.Header = assortPosition.Name;
            MessageBox.Show("Сохранено");
        }

        /// <summary>
        /// Обработчик удаления MasterPosition
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DeleteMasterHandler(object sender, EventArgs e)
        {
            var msgResult = MessageBox.Show("Вы действительно хотите удалить?", "Вопрос", MessageBoxButton.YesNo, MessageBoxImage.Information);
            if(msgResult == MessageBoxResult.Yes)
            {
                var item = (TreeViewItem)CategoriesTree.SelectedItem;
                var masterPosition = (Models.MasterCatalogChildren.MasterPosition)item.GetValue(MasterPositionDependencyProperty);

                var parentTree = (TreeViewItem)item.Parent;
                parentTree.Items.Remove(item);
                masterPosition.Parent.MasterPositions.Remove(masterPosition);
            }
        }
        /// <summary>
        /// Обработчик удаления Category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DeleteCategoryHandler(object sender, EventArgs e)
        {
            var msgResult = MessageBox.Show("Вы действительно хотите удалить?", "Вопрос", MessageBoxButton.YesNo, MessageBoxImage.Information);
            if (msgResult == MessageBoxResult.Yes)
            {
                var item = (TreeViewItem)CategoriesTree.SelectedItem;
                var category = (Models.MasterCatalogChildren.Category)item.GetValue(CategoryDependencyProperty);

                var parentTree = (TreeViewItem)item.Parent;
                parentTree.Items.Remove(item);
                _masterCatalog.Categories.Remove(category);
            }
        }
        /// <summary>
        /// Обработчик удаления AssortPosition
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DeleteAssortHandler(object sender, EventArgs e)
        {
            var msgResult = MessageBox.Show("Вы действительно хотите удалить?", "Вопрос", MessageBoxButton.YesNo, MessageBoxImage.Information);
            if (msgResult == MessageBoxResult.Yes)
            {
                var item = (TreeViewItem)CategoriesTree.SelectedItem;
                var assortPosition = (Models.MasterCatalogChildren.AssortPosition)item.GetValue(AssortPositionDependencyProperty);

                var parentTree = (TreeViewItem)item.Parent;
                parentTree.Items.Remove(item);
                assortPosition.Parent.AssortPositions.Remove(assortPosition);
            }
        }
    }
}
